window.addEventListener('scroll', function() {
    var aboutSection = document.getElementById('about');
    var portfolioSection = document.getElementById('portfolio');
    var contactSection = document.getElementById('contact');
    
    if (isElementInViewport(aboutSection)) {
      aboutSection.classList.add('show');
    }
    
    if (isElementInViewport(portfolioSection)) {
      portfolioSection.classList.add('show');
    }
    
    if (isElementInViewport(contactSection)) {
      contactSection.classList.add('show');
    }
  });
  
  function isElementInViewport(element) {
    var rect = element.getBoundingClientRect();
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  }
  